CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Caminar is a simple, photo-oriented landing page with a lightbox gallery


REQUIREMENTS
------------

This theme require the following theme:

 * Bootstrap (https://www.drupal.org/project/bootstrap)


INSTALLATION
------------

As this theme is based on bootstrap you should download and enable
Drupal Bootrap project.

Steps for smooth installation of caminar theme:
  - Theme file can be downloaded from the link
    https://www.drupal.org/project/caminar - Extract the downloaded
    file to the themes directory.

  - Goto Admin > Appearance, find caminar theme and choose 'Install and
    set as default' option.

  - You have now enabled your theme.


CONFIGURATION
-------------

The theme sections can be customized from the theme settings in
admin area.


MAINTAINERS
-----------

Current maintainer:
 * zyxware - https://www.drupal.org/u/zyxware
